# Greeleaf Media Toolkit

Symfony Console programs to make development go a little bit quicker.

## To Run
You can run from the command line by using the following in your project root directory.
`vendor/bin/grlf`
You can make an alias to `grlf` by typing `alias grlf="vendor/bin/grlf"`

### Commands
- `init` - Initializes the .grlf configuration file.  You'll need to fill in the approiate details.
- `db:backup` - Will create a DB backup in the current directory.
- `db:upload` - Will call `db:backup` and then upload the resulting file to S3.

### Environment File
You must initialize the .grlf file before running commands.

##  Setting Up AWS
### Create a folder for the project
Log into AWS S3 and create a project folder in your bucket set aside for recent backups.  The project name should be the same as the git repo name without the `.git`.  It is used throughout the system.

### Set up proper permissions for the folder
Log into IAM Policy and create a user.  This user will have the KEY and SECRET needed for placing in the `.grlf` config file.

####  To only allow access to one folder (for the live server that is going to be backed up):
 
```
{
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": [
                "arn:aws:s3:::s3-bucket-name/projectname/*"
            ]
        }
    ]
}
```

#### To create a user for your local environment


 

