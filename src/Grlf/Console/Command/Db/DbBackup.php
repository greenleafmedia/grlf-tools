<?php
/**
 * Greenleaf Tools
 *
 * (The MIT license)
 * Copyright (c) 2016 Matt Scott
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated * documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * @package    Grlf
 * @subpackage Grlf\Console
 */
namespace Grlf\Console\Command\Db;

use Grlf\Console\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class DbBackup extends AbstractCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('db:backup')
            ->setDescription('Saves the current Joomla DB to a file')
            ->addArgument('file_name', InputArgument::OPTIONAL, 'Please enter name for saved sql dump')
            ->addOption(
                'force',
                'f',
                InputOption::VALUE_NONE
            )
            ->addOption(
                'path',
                null,
                InputOption::VALUE_REQUIRED,
                'Path to the folder where the CMS configuration is located'
            )
            ->setHelp(sprintf(
                '%sSaves the current Joomla DB to a file%s',
                PHP_EOL,
                PHP_EOL
            ));
    }

    /**
     * Initializes the application.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return bool|int|null
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     *
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->bootstrap($input, $output, ['cms','grlf']);

        $this->io->section("Greenleaf Database Backup");

        $file_name = $input->getArgument('file_name');

        if (null === $file_name) {
            $file_name = $this->getCmsConfig()->getDbName() . '.sql';
        }

        $file_path = $_SERVER['PWD'] . DIRECTORY_SEPARATOR . $file_name;
        $file_status = self::STATUS_RUN; //Start out assuming we're going to have to generate the file.

        if (file_exists($file_path) &&
            !$input->getOption('force')
        ) {
            $options = $this->getStatusText();

            $file_status = array_search(
               $this->io->choice(
                    $file_path . ' already exists.  What would you like to do?',
                    $options,
                    $options[self::STATUS_ABORT]
                ),
                $options
            );

            if ($file_status == self::STATUS_ABORT) {
                $this->io->warning("Database was not backed up.");
                return $file_status;
            }
        }

        if ($file_status == self::STATUS_RUN) {
            $this->io->text("Dumping DB...");

            $command = "mysqldump -u " . $this->getCmsConfig()->getDbUser() . ' -p"' . $this->getCmsConfig()->getDbPassword() .
                '" ' . $this->getCmsConfig()->getDbName() . ' > ' . $file_path;

            $process = new Process($command);
            $process->run();

            $this->io->success("Database dump complete.  SQL file can be found at " . $file_path);
        }

        return $file_status;
    }
}
