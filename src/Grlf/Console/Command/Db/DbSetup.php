<?php
namespace Grlf\Console\Command\Db;

use Aws\Credentials\CredentialProvider;
use Aws\S3\S3Client;
use Grlf\Console\Command\AbstractCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class DbSetup extends AbstractCommand
{

    protected $sqlfile_name;
    protected $sqlfile_path;

    protected function configure()
    {
        $this
            ->setName('db:setup')
            ->setDescription('Setup a local database clone for development.')
            ->addArgument('project', InputArgument::REQUIRED, 'The folder name of project (should be same as .git repo)')
            ->setHelp(
                "The project argument is extremely important as it is reused many times throughout " .
                "the Greenleaf Tools environment.  It is used as reference on AWS and for the local " .
                "domain.  In order for all setup to happen automatically it should be named the same " .
                "as the git repository (without the .git file extension)."
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->bootstrap($input, $output, ['cms', 'grlf']);
        $this->sqlfile_name = $this->getCmsConfig()->getDbName() . '.sql';
        $this->sqlfile_path = $_SERVER['PWD'] . DIRECTORY_SEPARATOR . $this->sqlfile_name;

        $this->io->title("Setup database for local development.");

        $this->downloadFromAws();

        $this->replaceDomain();

        $this->syncLocalDb();

        $this->io->success("Database Setup Complete.");
        return true;
    }

    protected function downloadFromAws()
    {
        $this->io->section("Downloading most recent database...");

        $s3 = new S3Client([
            'region' => $this->getGrlfConfig()->s3->region,
            'version' => '2006-03-01',
            'credentials' => CredentialProvider::env()
        ]);

        try {
            $bucket = $this->getGrlfConfig()->s3->bucket;
            $keyname = $this->input->getArgument('project') . DIRECTORY_SEPARATOR . $this->sqlfile_name;

            //Get Info of Item
            $info = $s3->headObject([
                'Bucket' => $bucket,
                'Key' => $keyname
            ]);

            $file_size = $info['ContentLength'];

            //Progress bar setup
            $progress_bar = new \stdClass();
            $progress_bar->steps = (int)ceil($file_size / self::FILE_CHUNK);
            $progress_bar->bar = $this->io->createProgressBar($progress_bar->steps);

            //Style
            $progress_bar->bar->setFormat('very_verbose');
            $progress_bar->bar->start();

            //Download from S3 in chunks to show progress
            $start_range = 0;
            $end_range = self::FILE_CHUNK;


            unlink($this->sqlfile_path);
            for ($i =0 ; $i < $progress_bar->steps; $i++) {
                $progress_bar->bar->advance();

                $handle = fopen($this->sqlfile_path, 'a+');

                $range = 'bytes=' . $start_range . '-' . $end_range;
                // Get a range of bytes from an object.
                $result = $s3->getObject(array(
                    'Bucket' => $bucket,
                    'Key'    => $keyname,
                    'Range'  => $range,
                    'SaveAs' => $handle
                ));

                fclose($handle);
                $start_range = $end_range + 1;
                $end_range = $start_range + self::FILE_CHUNK;
            }

            $progress_bar->bar->finish();
            $this->io->newLine(2);
        } catch (\Exception $e) {
        }
    }

    protected function replaceDomain()
    {
        $this->io->section("Replacing Production URL with our dev one...");

        $old_domain = $this->io->ask("What is the production URL (no 'http'.  Include any www)?");
        $command = "sed -i '' 's/" . $old_domain . "/www." .
            $this->input->getArgument('project'). ".dev/g' " . $this->sqlfile_path;

        $this->io->text("Replacing " . $old_domain . "...");
        $process = new Process($command);
        $process->run();
    }

    protected function syncLocalDb()
    {
        $this->io->section("Updating local db with new file...");

        $command = "mysql -u " . $this->getCmsConfig()->getDbUser() . " -p\"" .
            $this->getCmsConfig()->getDbPassword() . "\" " . $this->getCmsConfig()->getDbName() .
            " < " . $this->sqlfile_path;

        $this->io->text("Executing sql file...");
        $process = new Process($command);
        $process->run();
    }
}
