<?php
namespace Grlf\Console\Command\Db;

use Aws\Credentials\CredentialProvider;
use Aws\S3\Exception\S3Exception;
use Aws\S3\MultipartUploader;
use Aws\S3\S3Client;
use Grlf\Console\Command\AbstractCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DbUpload extends AbstractCommand
{

    protected function configure()
    {
        $this
            ->setName('db:upload')
            ->setDescription('Create a database backup and upload to AWS.')
            ->addArgument('project', InputArgument::REQUIRED, 'The folder name of project (should be same as .git repo')
            ->addOption(
                'force',
                'f',
                InputOption::VALUE_NONE
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->bootstrap($input, $output, ['cms', 'grlf']);

        $this->io->title("Backup CMS Database to AWS");

        //Make a backup
        $command = $this->getApplication()->find('db:backup');

        $this->io->text("Calling db:backup...");
        $backup_args = array();
        if ($input->getOption('force')) {
            $backup_args['--force'] = true;
        }

        $backup_input = new ArrayInput($backup_args);
        $returnCode = $command->run($backup_input, $output);

        if ($returnCode == self::STATUS_ABORT) {
            return $returnCode;
        }

        $this->io->section("Upload to AWS");

        $s3 = new S3Client([
            'region' => $this->getGrlfConfig()->s3->region,
            'version' => '2006-03-01',
            'credentials' => CredentialProvider::env()
        ]);


        try {
            $file_name = $this->getCmsConfig()->getDbName() . '.sql';
            $file_path = $_SERVER['PWD'] . DIRECTORY_SEPARATOR . $file_name;

            //Progress bar setup
            $progress_bar = new \stdClass();
            $progress_bar->steps = (int) floor(filesize($file_path)/ self::FILE_CHUNK);
            $progress_bar->bar = $this->io->createProgressBar($progress_bar->steps);

            //Style
            $progress_bar->bar->setFormat('very_verbose');
            $progress_bar->bar->start();
            $uploader = new MultipartUploader($s3, fopen($file_path, 'rb'), [
                'Bucket' => $this->getGrlfConfig()->s3->bucket,
                'Key' => $input->getArgument('project') . DIRECTORY_SEPARATOR . $file_name,
                'before_upload' => function () use ($progress_bar) {
                    $progress_bar->bar->advance();
                },
            ]);

            $result = $uploader->upload();

            $progress_bar->bar->finish();
            $this->io->newLine(2);

            //Delete file
            if ($returnCode == self::STATUS_RUN) {
                unlink($file_path);
            }

            $this->io->success('File successfully uploaded to AWS S3');
        } catch (S3Exception $e) {
            die(var_dump($e));
        }

        $this->io->success("AWS Upload Complete.");
        return true;
    }
}
