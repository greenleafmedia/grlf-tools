<?php
/**
 * Greenleaf Tools
 *
 * (The MIT license)
 * Copyright (c) 2016 Matt Scott
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated * documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * @package    Grlf
 * @subpackage Grlf\Console
 */
namespace Grlf\Console\Command;

use Grlf\Config\CmsConfigInterface;
use Grlf\Config\JoomlaConfig;
use SSD\DotEnv\DotEnv;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Abstract command, contains bootstrapping info
 *
 */
abstract class AbstractCommand extends Command
{

    protected $cms_config;

    protected $input;
    protected $output;
    protected $io;

    const STATUS_RUN = 1;
    const STATUS_SKIP = 2;
    const STATUS_ABORT = 3;

    const FILE_CHUNK = 5242880;

    public function getStatusText()
    {
        return array(
            self::STATUS_RUN => 'Overwrite the existing file',
            self::STATUS_SKIP => "Continue using current file, don't overwrite",
            self::STATUS_ABORT => 'Abort this process'
        );
    }

    /**
     * Bootstrap Grlf.
     * @param $types
     * @throws \Exception
     */
    public function bootstrap(InputInterface $input, OutputInterface $output, $types)
    {
        if (!is_array($types)) {
            throw new \Exception("types must be of type Array");
        }

        if (in_array('grlf', $types) && !$this->getGrlfConfig()) {
            $this->loadGrlfConfig();
        }

        if (in_array('cms', $types) && !$this->getCmsConfig()) {
            $this->loadCmsConfig();
        }

        $this->input = $input;
        $this->output = $output;
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * Sets the config.
     *
     * @param CmsConfigInterface $config
     * @return AbstractCommand
     */
    public function setCmsConfig(CmsConfigInterface $config)
    {
        $this->cms_config = $config;
        return $this;
    }

    /**
     * Gets the config.
     *
     * @return CmsConfigInterface
     */
    public function getCmsConfig()
    {
        return $this->cms_config;
    }

    /**
     * Parse the config file and load it into the config object
     *
     * @throws \InvalidArgumentException
     * @return void
     */
    protected function loadCmsConfig()
    {
        $config = new JoomlaConfig($this->getGrlfConfig()->cms_path);

        $this->setCmsConfig($config);
    }

    /**
     * Sets the config.
     *
     * @return AbstractCommand
     */
    public function setGrlfConfig(\stdClass $config)
    {
        $this->grlf_config = $config;
        return $this;
    }

    /**
     * Gets the config.
     *
     */
    public function getGrlfConfig()
    {
        return $this->grlf_config;
    }

    /**
     * Parse the config file and load it into the config object
     *
     * @throws \InvalidArgumentException
     * @return void
     */
    protected function loadGrlfConfig()
    {
        $dotenv = new DotEnv($_SERVER['PWD']  . '/.grlf');
        $dotenv->load();

        $config = new \stdClass();

        //AWS Settings
        $s3 = new \stdClass();
        $s3->bucket = DotEnv::get('S3_BUCKET');
        $s3->region = DotEnv::get('S3_REGION');
        $s3->key_id = DotEnv::get('AWS_ACCESS_KEY_ID');
        $s3->secret = DotEnv::get('AWS_SECRET_ACCESS_KEY');

        $config->s3 = $s3;

        //Configuration file path
        $config->cms_path = DotEnv::get('CMS_PATH');

        //Check path for trailing slash
        if ($config->cms_path && substr($config->cms_path,-1) != DIRECTORY_SEPARATOR) {
            $config->cms_path .= DIRECTORY_SEPARATOR;
        }

        $this->setGrlfConfig($config);
    }
}
