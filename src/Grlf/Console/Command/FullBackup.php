<?php
/**
 * Grlf
 *
 * (The MIT license)
 * Copyright (c) 2016 Matt Scott
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated * documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * @package    Grlf
 * @subpackage Grlf\Console
 */
namespace Grlf\Console\Command;

use Grlf\AwsHelper;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FullBackup extends AbstractCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('full-backup')
            ->setDescription('Backs up files and database to AWS.')
            ->setHelp(sprintf(
                '%sCreates a backup of the database, zips all files, and then uploads to AWS.%s',
                PHP_EOL,
                PHP_EOL
            ));
    }

    /**
     * Runs a backup.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->bootstrap($input, $output, ['cms', 'grlf']);

        //Set timer
        $start_mark = time();
        $this->io->text("Backup process started at: " . date("h:i:s"));


        $this->io->title("Create Full Backup and upload to AWS");

        //Make a backup
        $command = $this->getApplication()->find('db:backup');

        $this->io->text('Backing up db...');

        $sql_name = time() . '_' . $this->getCmsConfig()->getDbName() . '.sql';

        $backup_args = array(
            '--force' => true,
            'file_name' => $sql_name
        );

        $backup_input = new ArrayInput($backup_args);
        $returnCode = $command->run($backup_input, $output);

        if ($returnCode == self::STATUS_ABORT) {
            return $returnCode;
        }

        //Move SQL backup down into directory to be zipped
        rename(
            $_SERVER['PWD'] . DIRECTORY_SEPARATOR . $sql_name,
            $_SERVER['PWD'] . DIRECTORY_SEPARATOR . $this->getGrlfConfig()->cms_path . $sql_name
        );

        $zip_path = $_SERVER['PWD'] . DIRECTORY_SEPARATOR . 'latest.tar.gz';

        //Compress the file structure
        $this->io->section("Compressing files...");
        exec(
            'tar --exclude=\'.git\' --exclude=\'.idea\' --exclude=\'node_modules\' --exclude=\'./vendor\' --exclude=\'tmp/\' -zcvf ' . $zip_path . ' ' . $this->getGrlfConfig()->cms_path
        );

        //Upload the gzipped file to AWS
        $this->io->section("Upload to AWS");
        AwsHelper::upload($this->getGrlfConfig()->s3, $zip_path, $this->io);

        //Clean up
        $this->io->section("Clean up");
        unlink($zip_path);
        unlink($_SERVER['PWD'] . DIRECTORY_SEPARATOR . $this->getGrlfConfig()->cms_path . $sql_name);

        $this->io->text("Backup took " . date("H:i:s", (time() - $start_mark) . " seconds."));
        return 1;
    }
}
