<?php
/**
 * Grlf
 *
 * (The MIT license)
 * Copyright (c) 2016 Matt Scott
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated * documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * @package    Grlf
 * @subpackage Grlf\Console
 */
namespace Grlf\Console\Command;

use Grlf\AwsHelper;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class SyncDirectory extends AbstractCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('sync-directory')
            ->setDescription('Syncs directory to AWS.')
            ->setHelp(sprintf(
                '%sSyncs a directory to AWS.%s',
                PHP_EOL,
                PHP_EOL
            ))
            ->addArgument('directory',
            			InputArgument::REQUIRED,
            			'Local Directory to sync'
            )
            ->addArgument('virtual-directory',
            			InputArgument::OPTIONAL,
            			'Virtual Directory to sync to.'
            );
    }

    /**
     * Sync a directory.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->bootstrap($input, $output, ['cms', 'grlf']);

        //Set timer
        $start_mark = time();
        $this->io->text("Sync process started at: " . date("h:i:s"));


        $this->io->title("Create Full Backup and upload to AWS");

        
        //Upload the gzipped file to AWS
        $this->io->section("Upload to AWS");
        AwsHelper::uploadDirectory($this->getGrlfConfig()->s3, $input->getArgument('directory'), $input->getArgument('virtual-directory', ''), $this->io);


        $this->io->text("Backup took " . date("H:i:s", (time() - $start_mark) . " seconds."));
        return 1;
    }
}
