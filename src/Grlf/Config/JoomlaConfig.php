<?php
/**
 * Greenleaf Tools
 *
 * (The MIT license)
 * Copyright (c) 2016 Matt Scott
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated * documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * @package    Grlf
 * @subpackage Grlf\Config
 */
namespace Grlf\Config;

/**
 * Joomla configuration class.
 *
 * @package Grlf
 * @author Matt Scott
 */
class JoomlaConfig implements CmsConfigInterface
{

    protected $cms_config;

    /**
     * @var string
     */
    public function __construct($path = '')
    {
        $filepath = $_SERVER['PWD'] . DIRECTORY_SEPARATOR . $path . 'configuration.php';
        //Include for file
        if (file_exists($filepath)) {
            require_once $filepath;
        } else {
            throw new JoomlaConfigNotFoundException;
        }

        //Check for Joomla config
        if (!class_exists('JConfig')) {
            throw new JoomlaConfigNotFoundException;
        }

        $this->cms_config = new \JConfig();
    }

    public function getDbName()
    {
        return $this->cms_config->db;
    }

    public function getDbUser()
    {
        return $this->cms_config->user;
    }

    public function getDbPassword()
    {
        return $this->cms_config->password;
    }
}
