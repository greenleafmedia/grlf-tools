<?php
/**
 * Greenleaf Tools
 *
 * (The MIT license)
 * Copyright (c) 2016 Matt Scott
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated * documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * @package    Grlf
 * @subpackage Grlf\Config
 */
namespace Grlf\Config;

/**
 * Grlf configuration interface.
 *
 * @package Grlf
 */
interface CmsConfigInterface
{
    /**
     * Class Constructor
     *
     * @param null $configFilename
     */
    public function __construct($configFilename = null);


    /**
     * Gets the CMS database name.
     *
     * @return string
     */
    public function getDbName();

    /**
     * Gets the CMS DB Username
     *
     * @return string
     */
    public function getDbUser();

    /**
     * Gets the CMS DB Password
     *
     * @return string
     */
    public function getDbPassword();
}
