<?php
/**
 * Greenleaf Tools
 *
 * (The MIT license)
 * Copyright (c) 2016 Matt Scott
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated * documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * @package    Grlf
 */
namespace Grlf;

use Aws\Credentials\CredentialProvider;
use Aws\S3\MultipartUploader;
use Aws\S3\S3Client;

/**
 * Help for AWS interactions
 *
 * @package     Grlf
 *
 * @since version
 */
class AwsHelper {
    const FILE_CHUNK = 26214400;

    public static function upload($s3_config, $file, $io = null) {

        $s3 = new S3Client([
            'region' => $s3_config->region,
            'version' => '2006-03-01',
            'credentials' => CredentialProvider::env()
        ]);


        try {
            $progress_bar = null;
            if ($io) {
                //Progress bar setup
                $progress_bar = new \stdClass();
                $progress_bar->steps = (int) floor(filesize($file)/ self::FILE_CHUNK);
                $progress_bar->bar = $io->createProgressBar($progress_bar->steps);

                //Style
                $progress_bar->bar->setFormat('very_verbose');
                $progress_bar->bar->start();
            }

            $uploader = new MultipartUploader($s3, fopen($file, 'rb'), [
                'Bucket' => $s3_config->bucket,
                'Key' => basename($file),
                'before_upload' => function () use ($progress_bar) {
                    //Clean up any garbage hanging out
                    gc_collect_cycles();
                    if ($progress_bar) {
                        $progress_bar->bar->advance();
                    }
                },
                'part_size' => 26214400
            ]);

            $result = $uploader->upload();

            if ($progress_bar) {
                $progress_bar->bar->finish();
                $io->newLine(2);
                $io->success('File successfully uploaded to AWS S3');
            }


        } catch (S3Exception $e) {
            die(var_dump($e));
        }

        if ($io) {
            $io->success("AWS Upload Complete.");
        }

    }
    
    public static function uploadDirectory($s3_config, $directory, $virtualDirectory, $io = null)
    {
	     $s3 = new S3Client([
            'region' => $s3_config->region,
            'version' => '2006-03-01',
            'credentials' => CredentialProvider::env()
        ]);


        try {
            $progress_bar = null;
            if ($io) {
                //Progress bar setup
                $progress_bar = new \stdClass();
                $progress_bar->steps = (int) floor(filesize($file)/ self::FILE_CHUNK);
                $progress_bar->bar = $io->createProgressBar($progress_bar->steps);

                //Style
                $progress_bar->bar->setFormat('very_verbose');
                $progress_bar->bar->start();
            }

            $s3->uploadDirectory($directory, $s3_config->bucket, $virtualDirectory);

        } catch (S3Exception $e) {
            die(var_dump($e));
        }

        if ($io) {
            $io->success("AWS Upload Complete.");
        }

    }
}